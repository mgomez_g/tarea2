import javafx.scene.chart.XYChart;

public class Pedestrian {
    private double x, y, speed, angle, deltaAngle, delta_t;
    private double x_tPlusDelta, y_tPlusDelta;
    private Comuna comuna;
    private char estado;
    private char estado_tPlusDelta;
    public PedestrianView view;

    public Pedestrian(Comuna comuna, double speed, double deltaAngle, char state) {
        double Size = PedestrianView.SIZE;
        estado = state;
        estado_tPlusDelta = state;
        angle = Math.random() * 2 * Math.PI;
        double width = SimulatorConfig.WIDTH;
        double largo = SimulatorConfig.LENGTH;
        this.x = (Size/2) + Math.random() * (width - Size);
        this.y = (Size/2) + Math.random() * (largo - Size);
        this.comuna = comuna;
        this.speed = speed;
        this.deltaAngle = deltaAngle;
        this.x_tPlusDelta = this.x;
        this.y_tPlusDelta = this.y;
        view = new PedestrianView(comuna, this);
    }
    public void restart(double speed, double deltaAngle, char state){
        this.speed = speed;
        this.deltaAngle = deltaAngle;
        estado = state;
        estado_tPlusDelta = state;
        double Size = PedestrianView.SIZE;
        double width = SimulatorConfig.WIDTH;
        double largo = SimulatorConfig.LENGTH;
        this.x = (Size/2) + Math.random() * (width - Size);
        this.y = (Size/2) + Math.random() * (largo - Size);
    }
    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public void computeNextState() {
        double r = Math.random();
        delta_t = SimulatorConfig.DELTA_T;
        angle += Math.random() < 0.5 ? -Math.random() * deltaAngle : Math.random() * deltaAngle;
        x_tPlusDelta = x + speed * delta_t * Math.cos(angle);
        y_tPlusDelta = y + speed * delta_t * Math.sin(angle);

        if (x_tPlusDelta < 0) {   // rebound logic: pared izquierda
            angle = Math.PI - angle;
            x_tPlusDelta = -x_tPlusDelta;
        } else if (x_tPlusDelta > comuna.getWidth() - 10) { // rebound logic: pared derecha
            x_tPlusDelta = 2 * comuna.getWidth() - x_tPlusDelta - 20;
            angle = Math.PI - angle;
        }
        if (y_tPlusDelta < 0) {   // rebound logic: pared inferior
            y_tPlusDelta = -y_tPlusDelta;
            angle = -angle;
        } else if (y_tPlusDelta > comuna.getHeight() - 10) {  // rebound logic: pared superior
            y_tPlusDelta = 2 * comuna.getHeight() - y_tPlusDelta - 20;
            angle = -angle;
        }
    }//se encarga de predecir la próxima posicion del individuo

    public void updateState() {
        x = x_tPlusDelta;
        y = y_tPlusDelta;
        estado = estado_tPlusDelta;
    }//actualiza los datos actuales por los del tipo tPlusDelta

    public double getX_tPlusDelta() {
        return this.x_tPlusDelta;
    }

    public double getY_tPlusDelta() {
        return this.y_tPlusDelta;
    }

    public double distanceTo(Pedestrian individuo) {
        double xf = individuo.getX();
        double yf = individuo.getY();
        double dx = (xf - this.x);
        double dy = (yf - this.y);
        double distance = Math.sqrt(dx * dx + dy * dy);
        return distance;
    }

    public boolean contactoEstrecho(Pedestrian individuo) {
        if (distanceTo(individuo) <= SimulatorConfig.D) {
            return true;
        } else {
            return false;
        }
    }

    public static void Exposicion(Pedestrian individuo1, Pedestrian individuo2) {//contagia susceptibles cuando existe contacto estrecho
        if (Math.random() < SimulatorConfig.P0) {
            if (individuo1.infectado() && individuo2.suceptible()) {
                individuo2.estado_tPlusDelta = 'i';
                individuo2.view.changeShape(individuo2.estado_tPlusDelta);
            }
            if (individuo2.infectado() && individuo1.suceptible()) {
                individuo1.estado_tPlusDelta = 'i';
                individuo1.view.changeShape(individuo1.estado_tPlusDelta);
            }
        }
    }

    public char getState() {
        return estado;
    }

    public boolean infectado() {
        return this.estado == 'i';
    }

    public boolean suceptible() {
        return this.estado == 's';
    }

}