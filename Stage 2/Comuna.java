import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.Pane;
import java.util.ArrayList;

public class Comuna {
    private Rectangle2D territory;
    private ComunaView view;
    private Pane graph;
    private ArrayList<Pedestrian> poblacion;

    public Comuna(){
        poblacion = null;
        double width = SimulatorConfig.WIDTH;
        double length = SimulatorConfig.LENGTH;
        territory = new Rectangle2D(0,0, width, length);
        view = new ComunaView(this); // What if you exchange this and the follow line?
        graph = new Pane();  // to be completed in other stages.
        poblacionBuilder();
    }
    public double getWidth() {
        return territory.getWidth();
    }
    public double getHeight() {
        return territory.getHeight();
    }
    private void poblacionBuilder(){
        poblacion = new ArrayList<Pedestrian>();
        for(int i=0; i<SimulatorConfig.N; i++){
            if(i<SimulatorConfig.I){
                poblacion.add(new Pedestrian(this, (Math.random()*0.2 + 0.9) * SimulatorConfig.SPEED, SimulatorConfig.DELTA_THETA, 'i'));//se agregan los infectados
            }else{
                poblacion.add(new Pedestrian(this, (Math.random()*0.2 + 0.9) * SimulatorConfig.SPEED, SimulatorConfig.DELTA_THETA, 's'));//se agregan los susceptibles
            }
        }
        setPoblacion(poblacion);
    }
    public void poblacionRestart(){
        for(int i=0; i<poblacion.size(); i++){
            if(i<SimulatorConfig.I){
                poblacion.get(i).restart((Math.random()*0.2 + 0.9) * SimulatorConfig.SPEED, SimulatorConfig.DELTA_THETA, 'i');//se agregan los infectados
                poblacion.get(i).view.changeShape(poblacion.get(i).getState());
            }else{
                poblacion.get(i).restart((Math.random()*0.2 + 0.9) * SimulatorConfig.SPEED, SimulatorConfig.DELTA_THETA, 's');//se agregan los susceptibles
                poblacion.get(i).view.changeShape(poblacion.get(i).getState());
            }
        }
    }
    public void computeNextState(){
        for (int i=0; i<poblacion.size(); i++ ){
            poblacion.get(i).computeNextState();
            for(int j=(i+1); j<poblacion.size(); j++){
                if(poblacion.get(i).contactoEstrecho(poblacion.get(j))){
                    Pedestrian.Exposicion(poblacion.get(j),poblacion.get(i));//para cada individuo se verifica si ha tenido contacto estrecho con cualquiera de los otros individuos
                }
            }
        }
    }
    public void updateState () {poblacion.forEach((Pedestrian) -> {Pedestrian.updateState();});}
    public void updateGraph(double simulationTime){
        int I = 0;
        int S = 0;
        for(int i=0; i<poblacion.size(); i++){
            I+= poblacion.get(i).infectado() ? 1:0;
            S+= poblacion.get(i).suceptible() ? 1:0;
        }
    }
    public void updateView(){view.update();}
    public void setPoblacion(ArrayList<Pedestrian> poblacion){this.poblacion = poblacion;}
    public ArrayList<Pedestrian> getPoblacion(){return poblacion;}
    public Group getView() {return view;}
 }
