import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;
import javafx.scene.Node;
import javafx.scene.shape.StrokeType;

public class PedestrianView {
    private Pedestrian person;
    public Node view;
    public static final double SIZE = 10;
    public Comuna comuna;
    public PedestrianView(Comuna comuna, Pedestrian p) {
        person = p;
        this.comuna = comuna;
        switch (p.getState()){
            case 's':
                view = new Rectangle(SIZE, SIZE, Color.BLUE);
                view.setTranslateX(person.getX() - SIZE / 2);
                view.setTranslateY(person.getY() - SIZE / 2);
                break;
            case 'i':
                view = new Circle(SIZE/2, Color.LIGHTCORAL);
                view.setTranslateX(person.getX());
                view.setTranslateY(person.getY());
                Circle viewN = (Circle) view;
                viewN.setStroke(Color.RED);
                viewN.setStrokeType(StrokeType.OUTSIDE);
                viewN.setStrokeWidth(2);
                view = viewN;
                break;}
        comuna.getView().getChildren().add(view);
    }
    public double getSIZE(){return SIZE;}
    public void update() {
            view.setTranslateX(person.getX());
            view.setTranslateY(person.getY());
    }
    public void changeShape(char to){
        switch (to){
            case 'i':
                Simulator.beep.play();
                comuna.getView().getChildren().remove(view);
                view = new Circle(5.0f, Color.LIGHTCORAL);
                Circle viewN = (Circle) view;
                viewN.setStroke(Color.RED);
                viewN.setStrokeType(StrokeType.OUTSIDE);
                viewN.setStrokeWidth(2);
                view = viewN;
                comuna.getView().getChildren().add(view);
                break;
            case 's':
                comuna.getView().getChildren().remove(view);
                view = new Rectangle(SIZE, SIZE, Color.BLUE);
                view.setTranslateX(person.getX() - SIZE / 2);
                view.setTranslateY(person.getY() - SIZE / 2);
                comuna.getView().getChildren().add((Rectangle) view);
                break;
        }}
}