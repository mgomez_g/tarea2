///////////////// Pedestrian.java /////////////////

/**
 * Clase que simula a un individuo
 * @author Ignacio Barrera, Diego Almonacid, Gabriela Bustamante, Mariapaz Gomez
 */


public class Pedestrian {

    private double x, y, speed, angle, deltaAngle, delta_t;
    private double x_tPlusDelta, y_tPlusDelta;
    private Comuna comuna;
    private char estado;
    private char estado_tPlusDelta;
    public PedestrianView view;
    private double infectadoLimitTime;
    private double infectadoTime;
    private final boolean mascarilla;

    /**
     * Constructor del Individuo
     *
     * @param comuna Comuna del Individuo.
     * @param speed Velocidad del Individuo.
     * @param deltaAngle Rango de angulos posibles para el individuo.
     * @param infectado_limit_time Tiempo que se demora el individuo en recuperarse.
     * @param state Estado del individuo (infectado, susceptible, recuperado, vacunado).
     * @param mascarilla Atributo de tipo booleano que indica si el Individuo tiene mascarilla o no.
     */


    public Pedestrian(Comuna comuna, double speed, double deltaAngle,double infectado_limit_time ,char state, boolean mascarilla) {
        double Size = PedestrianView.SIZE;
        estado = state;
        estado_tPlusDelta = state;
        angle = Math.random() * 2 * Math.PI;
        double width = SimulatorConfig.WIDTH;
        double largo = SimulatorConfig.LENGTH;
        this.x = (Size/2) + Math.random() * (width - Size);
        this.y = (Size/2) + Math.random() * (largo - Size);
        this.comuna = comuna;
        this.speed = speed;
        this.deltaAngle = deltaAngle;
        this.x_tPlusDelta = this.x;
        this.y_tPlusDelta = this.y;
        this.mascarilla = mascarilla;
        view = new PedestrianView(comuna, this);
        if(this.infectado()){
            initInfectadoTimer();
        }else{
            infectadoTime = -1;
        }
        this.infectadoLimitTime = infectado_limit_time;
    }

    /**
     * "Vacuna" al individuo en su siguiente instancia de tiempo
     */
    public void vacunarse(){ estado_tPlusDelta = 'v'; }

    /**
     * Inicia el contador de tiempo infectado del individuo
     */
    public void initInfectadoTimer(){ infectadoTime=0; }

    /**
     * Retorna el atributo X del individuo
     * @return atributo X
     */
    public double getX() { return this.x; }

    /**
     * Retorna el atributo Y del individuo
     * @return atributo Y
     */
    public double getY() { return this.y; }

    /**
     * Retorna el atributo X_tPlusDelta del individuo
     * @return atributo X_tPlusDelta
     */
    public double getX_tPlusDelta() { return this.x_tPlusDelta; }

    /**
     * Retorna el atributo Y_tPlusDelta del individuo
     * @return atributo Y_tPlusDelta
     */
    public double getY_tPlusDelta() { return this.y_tPlusDelta; }

    /**
     * Retorna el estado del individuo
     * @return atributo estado
     */
    public char getState() { return estado; }

    /**
     * Retorna true or false dependiendo del estado del Individuo (infectado,suscpetible,recuperado,vacunado)
     * @return true or false
     */
    public boolean infectado() { return this.estado == 'i'; }

    /**
     * Retorna true or false dependiendo del estado del Individuo (infectado,suscpetible,recuperado,vacunado)
     * @return true or false
     */
    public boolean suceptible() { return this.estado == 's'; }

    /**
     * Retorna true or false dependiendo del estado del Individuo (infectado,suscpetible,recuperado,vacunado)
     * @return true or false
     */
    public boolean recuperado() { return this.estado == 'r'; }

    /**
     * Retorna true or false dependiendo del estado del Individuo (infectado,suscpetible,recuperado,vacunado)
     * @return true or false
     */
    public boolean vacunado() { return this.estado == 'v'; }

    /**
     * Retorna true or false dependiendo si el individuo tiene o no mascarilla
     * @return true or false
     */
    public boolean withmask() { return this.mascarilla; }

    /**
     * Da inicio al cronometro del infectado simbolicamente cambiando el -1 por un 0
     * @param delta_t Valor de saltos de tiempo.
     */
    public void runInfectadoTime(double delta_t){
        if(infectadoTime + delta_t < infectadoLimitTime) {
            infectadoTime += delta_t;
        }else{
            infectadoTime = -1;
            this.estado_tPlusDelta = 'r';
            this.view.changeShape('r');
        }
    }

    /**
     * Se encarga de predecir la próxima posicion del individuo
     */
    public void computeNextState() {
        double r = Math.random();

        delta_t = SimulatorConfig.DELTA_T;
        angle += Math.random() < 0.5 ? -Math.random() * deltaAngle : Math.random() * deltaAngle;
        x_tPlusDelta = x + speed * delta_t * Math.cos(angle);
        y_tPlusDelta = y + speed * delta_t * Math.sin(angle);
        //infection logic
        if(infectadoTime>=0){
            runInfectadoTime(delta_t);
        }
        if (x_tPlusDelta < 0) {   // rebound logic: pared izquierda
            angle = Math.PI - angle;
            x_tPlusDelta = -x_tPlusDelta;
        } else if (x_tPlusDelta > comuna.getWidth() - 10) { // rebound logic: pared derecha
            x_tPlusDelta = 2 * comuna.getWidth() - x_tPlusDelta - 20;
            angle = Math.PI - angle;
        }
        if (y_tPlusDelta < 0) {   // rebound logic: pared inferior
            y_tPlusDelta = -y_tPlusDelta;
            angle = -angle;
        } else if (y_tPlusDelta > comuna.getHeight() - 10) {  // rebound logic: pared superior
            y_tPlusDelta = 2 * comuna.getHeight() - y_tPlusDelta - 20;
            angle = -angle;
        }
    }

    /**
     * Actualiza los datos actuales por los del tipo tPlusDelta
     */
    public void updateState() {
        x = x_tPlusDelta;
        y = y_tPlusDelta;
        if((estado_tPlusDelta == 'i') && (infectadoTime ==-1)){
            initInfectadoTimer();
        }
        estado = estado_tPlusDelta;
    }

    /**
     * Calcula la distancia entre el this.Individuo y un Individuo ingresado como parametro
     * @param individuo Individuo a comparar con el this.Individuo
     * @return El valor de la distancia entre Individuos
     */
    public double distanceTo(Pedestrian individuo) {
        double xf = individuo.getX();
        double yf = individuo.getY();
        double dx = (xf - this.x);
        double dy = (yf - this.y);
        double distance = Math.sqrt(dx * dx + dy * dy);
        return distance;
    }

    /**
     * Define si dos individuos estan a una distancia "peligrosa"
     * @param individuo Individuo al cual se medira la distancia con el this.Individuo
     * @return true or false, dependiendo si la distancia entre individuos es menor a SimulatorConfig.D
     */
    public boolean contactoEstrecho(Pedestrian individuo) {
        if (distanceTo(individuo) <= SimulatorConfig.D) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Se encarga de simular la exposición entre dos individuos en contacto estrecho considerando los tres casos
     * @param individuo1 Primer individuo a someter a la exposicion
     * @param individuo2 Segundo individuo a someter a la exposicion
     */
    public static void Exposicion(Pedestrian individuo1, Pedestrian individuo2){
        double probabilidad_0 = SimulatorConfig.P0;
        double probabilidad_1 = SimulatorConfig.P1;
        double probabilidad_2 = SimulatorConfig.P2;
        if(individuo1.mascarilla && individuo2.mascarilla){
            makeExposicion(individuo1, individuo2, probabilidad_2);
        }
        if((!individuo1.mascarilla && individuo2.mascarilla) || (individuo1.mascarilla && !individuo2.mascarilla)){
            makeExposicion(individuo1, individuo2, probabilidad_1);
        }
        if(!individuo1.mascarilla && !individuo2.mascarilla){
            makeExposicion(individuo1, individuo2, probabilidad_0);
        }
    }

    /**
     * Se encarga de ejecutar la exposicion, e infectar al individuo correspondente sólo si es suceptible y las probabilidades lo indican
     * @param individuo1 Primer individuo a someter a la exposicion
     * @param individuo2 Segundo individuo a someter a la exposicion
     * @param P Probabilidad a infectarse
     */
    public static void makeExposicion(Pedestrian individuo1, Pedestrian individuo2, double P){
        if(Math.random() < P){
            if(individuo1.infectado() && individuo2.suceptible()){
                individuo2.estado_tPlusDelta = 'i';
                individuo2.view.changeShape(individuo2.estado_tPlusDelta);
            }
            if(individuo2.infectado() && individuo1.suceptible()){
                individuo1.estado_tPlusDelta = 'i';
                individuo1.view.changeShape(individuo1.estado_tPlusDelta);
            }
        }
    }

}