///////////////// PedestrianView.Java /////////////////

/**
 * Clase que simula la vista de un individuo
 * @author Ignacio Barrera, Diego Almonacid, Gabriela Bustamante, Mariapaz Gomez
 */


import javafx.animation.Animation;
import javafx.animation.FillTransition;
import javafx.scene.shape.*;
import javafx.scene.paint.Color;
import javafx.scene.Node;
import javafx.util.Duration;

public class PedestrianView {
    private final Pedestrian person;
    public Node view;
    public static final double SIZE = 10;
    public Comuna comuna;

    /**
     * PedestrianView Constructor
     * @param comuna comuna a la cual pertenece la vista
     * @param p Individuo a la cual pertenece la vista
     */
    public PedestrianView(Comuna comuna, Pedestrian p) {
        person = p;
        this.comuna = comuna;
        switch (p.getState()){
            case 's':
                if (p.withmask()) {
                    view = new Rectangle(SIZE, SIZE, Color.BLUE);
                    Rectangle viewN = (Rectangle) view;
                    viewN.setStrokeType(StrokeType.OUTSIDE);
                    viewN.setStroke(Color.BLACK);
                    viewN.setStrokeWidth(2);
                    view = viewN;
                    view.setTranslateX(person.getX() - SIZE / 2);
                    view.setTranslateY(person.getY() - SIZE / 2);
                }else{
                    view = new Rectangle(SIZE, SIZE, Color.BLUE);
                    view.setTranslateX(person.getX() - SIZE / 2);
                    view.setTranslateY(person.getY() - SIZE / 2);
                    }
                break;
            case 'i':
                view = new Circle(SIZE/2, Color.LIGHTCORAL);
                view.setTranslateX(person.getX());
                view.setTranslateY(person.getY());
                Circle viewN = (Circle) view;
                FillTransition transition = new FillTransition(Duration.millis(1000), viewN, Color.DARKRED, Color.RED);
                transition.setCycleCount(Animation.INDEFINITE);
                transition.setAutoReverse(true);
                transition.play();
                view = viewN;
                break;}
        comuna.getView().getChildren().add(view);
    }

    /**
     * Actualiza la vista segun la posicion actual del individuo
     */
    public void update() {
            view.setTranslateX(person.getX());
            view.setTranslateY(person.getY());
    }

    /**
     * Genera el cambio de figura de la vista
     * @param to Indica a que shape debe actualizarse la vista actual
     */
    public void changeShape(char to){
        switch (to){
            case 'i':
                Simulator.beep.play();
                comuna.getView().getChildren().remove(view);
                view = new Circle(5.0f, Color.LIGHTCORAL);
                Circle viewN = (Circle) view;
                FillTransition transition = new FillTransition(Duration.millis(1000), viewN, Color.DARKRED, Color.RED);
                transition.setCycleCount(Animation.INDEFINITE);
                transition.setAutoReverse(true);
                transition.play();
                view = viewN;
                comuna.getView().getChildren().add(view);
                break;
            case 's':
                comuna.getView().getChildren().remove(view);
                view = new Rectangle(SIZE, SIZE, Color.BLUE);
                view.setTranslateX(person.getX() - SIZE / 2);
                view.setTranslateY(person.getY() - SIZE / 2);
                comuna.getView().getChildren().add( view);
                break;
            case 'r':
                comuna.getView().getChildren().remove(view);
                view = new Rectangle(SIZE, SIZE, Color.BROWN);
                view.setTranslateX(person.getX() - SIZE/2);
                view.setTranslateY(person.getY() - SIZE / 2);
                comuna.getView().getChildren().add((Rectangle) view);
                break;
            case 'v':
                comuna.getView().getChildren().remove(view);
                Polygon polygon = new Polygon();
                polygon.getPoints().addAll( new Double[]{0.0, 0.0, 0.0, SIZE, SIZE,SIZE/2} );
                polygon.setFill(Color.GREEN);
                view = polygon;
                view.setTranslateX(person.getX() - SIZE/2);
                view.setTranslateY(person.getY() - SIZE/2);
                comuna.getView().getChildren().add((Polygon) view);
                break;

        }}
}