import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

public class SimulatorMenuBar extends MenuBar {
    SimulatorMenuBar (Simulator simulator){
        Menu controlMenu = new Menu("Control");
        Menu settingsMenu = new Menu("Settings");
        getMenus().addAll(controlMenu,settingsMenu);
        MenuItem start = new MenuItem("Start");
        MenuItem stop = new MenuItem("Stop");
        controlMenu.getItems().addAll(start,stop);
        start.setOnAction(e -> simulator.start());
        stop.setOnAction(e -> simulator.stop());
        }
}
