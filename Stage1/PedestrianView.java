import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;

public class PedestrianView {
    private Pedestrian person;
    public Rectangle view;
    private final double SIZE = 10;
    public PedestrianView(Comuna comuna, Pedestrian p) {
        person = p;
        view = new Rectangle(SIZE, SIZE, Color.BLUE);
        view.setX(person.getX()-SIZE/2);
        view.setY(person.getY()+SIZE/2);
        comuna.getView().getChildren().add(view);
    }
    public void update() {
        view.setX(person.getX());
        view.setY(person.getY());
    }
}