# Tarea 2
Puesto que el ayudante nos comento que crearamos carpetas para la siguienet entrega, en esta oportunidada no separamos la tarea en branches. Para la creacion de este codigo, nos basamos en los codigos propuestos. 

> *Como grupo si decidimos optar por la puntuacion extra.* 

### Archivos del programa

* Stage1.java
* Comuna.java
* ComunaView.java
* Pedestrian.java
* PedestrianView.txt
* Simulator.java
* SimulatorConfig.java
* SimulatorMenuBar.java
 

### Requisitos :
*  Para asegurar el funcionamiento de nuestra tarea se recomienda usar SDK 16
* En caso de presentar algun error, este puede ser ocacionado por el archivo de entrada, ya que algunos sistemas funcionan con . y otros con , .

### Make 
Ya que utilizamos Makefile, deberas modificar el makefile con tu direccion de javaFx de la siguiente forma:
 ```Makefile
 doc: 
	javadoc --module-path [tu ubicacion de javaFx]/lib --add-modules javafx.controls,javafx.fxml,javafx.media *.java
	javac --module-path [tu ubicacion de javaFx]/lib --add-modules javafx.controls,javafx.fxml,javafx.media *.java

run: 
	java --module-path [tu ubicacion de javaFx]/lib --add-modules javafx.controls,javafx.fxml,javafx.media Stage1 'archivo de entrada.txt' 'archivo de audio.txt'

```
Una vez hecho esto deberia funcionar sin problemas.


> Para compilar debemos utilizar el comando 
```bash
$ make doc
```
> y luego para correr el programa  
```bash
$ make run
```

### Integrantes del equipo:
* Diego Almonacid
* Gabriela Bustamante 
* Ignacio Barrera
* Mariapaz Gómez
