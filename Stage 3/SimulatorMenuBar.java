import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

public class SimulatorMenuBar extends MenuBar {
    public static Menu setMenu;
    SimulatorMenuBar (Simulator simulator){
        Menu controlMenu = new Menu("Control");
        Menu settingsMenu = new Menu("Settings");
        getMenus().addAll(controlMenu,settingsMenu);
        setMenu = settingsMenu;
        MenuItem exit = new MenuItem("Exit");
        MenuItem start = new MenuItem("Start");
        MenuItem stop = new MenuItem("Stop");
        MenuItem parametros = new MenuItem("Parametros");
        controlMenu.getItems().addAll(start,stop,exit);

        settingsMenu.getItems().addAll(parametros);
        parametros.setOnAction(e -> simulator.MenuParametros());
        start.setOnAction(e -> simulator.start());
        stop.setOnAction(e -> simulator.stop());
        exit.setOnAction(e -> System.exit(0));
        }
}
