public class Pedestrian {

    private double x, y, speed, angle, deltaAngle, delta_t;
    private double x_tPlusDelta, y_tPlusDelta;
    private Comuna comuna;
    private char estado;
    private char estado_tPlusDelta;
    public PedestrianView view;
    private double infectadoLimitTime;
    private double infectadoTime;
    private final boolean mascarilla;

    public Pedestrian(Comuna comuna, double speed, double deltaAngle,double infectado_limit_time ,char state, boolean mascarilla) {
        double Size = PedestrianView.SIZE;
        estado = state;
        estado_tPlusDelta = state;
        angle = Math.random() * 2 * Math.PI;
        double width = SimulatorConfig.WIDTH;
        double largo = SimulatorConfig.LENGTH;
        this.x = (Size/2) + Math.random() * (width - Size);
        this.y = (Size/2) + Math.random() * (largo - Size);
        this.comuna = comuna;
        this.speed = speed;
        this.deltaAngle = deltaAngle;
        this.x_tPlusDelta = this.x;
        this.y_tPlusDelta = this.y;
        this.mascarilla = mascarilla;
        view = new PedestrianView(comuna, this);
        if(this.infectado()){
            initInfectadoTimer();
        }else{
            infectadoTime = -1;
        }
        this.infectadoLimitTime = infectado_limit_time;
    }

    public void vacunarse(){ estado_tPlusDelta = 'v'; }

    public void initInfectadoTimer(){ infectadoTime=0; }

    public double getX() { return this.x; }

    public double getY() { return this.y; }

    public double getX_tPlusDelta() { return this.x_tPlusDelta; }

    public double getY_tPlusDelta() { return this.y_tPlusDelta; }


    public char getState() { return estado; }

    public boolean infectado() { return this.estado == 'i'; }

    public boolean suceptible() { return this.estado == 's'; }

    public boolean recuperado() { return this.estado == 'r'; }

    public boolean vacunado() { return this.estado == 'v'; }

    public boolean withmask() { return this.mascarilla; }

    public void runInfectadoTime(double delta_t){
        if(infectadoTime + delta_t < infectadoLimitTime) {
            infectadoTime += delta_t;
        }else{
            infectadoTime = -1;
            this.estado_tPlusDelta = 'r';
            this.view.changeShape('r');
        }
    }

    public void computeNextState() {
        double r = Math.random();

        delta_t = SimulatorConfig.DELTA_T;
        angle += Math.random() < 0.5 ? -Math.random() * deltaAngle : Math.random() * deltaAngle;
        x_tPlusDelta = x + speed * delta_t * Math.cos(angle);
        y_tPlusDelta = y + speed * delta_t * Math.sin(angle);
        //infection logic
        if(infectadoTime>=0){
            runInfectadoTime(delta_t);
        }
        if (x_tPlusDelta < 0) {   // rebound logic: pared izquierda
            angle = Math.PI - angle;
            x_tPlusDelta = -x_tPlusDelta;
        } else if (x_tPlusDelta > comuna.getWidth() - 10) { // rebound logic: pared derecha
            x_tPlusDelta = 2 * comuna.getWidth() - x_tPlusDelta - 20;
            angle = Math.PI - angle;
        }
        if (y_tPlusDelta < 0) {   // rebound logic: pared inferior
            y_tPlusDelta = -y_tPlusDelta;
            angle = -angle;
        } else if (y_tPlusDelta > comuna.getHeight() - 10) {  // rebound logic: pared superior
            y_tPlusDelta = 2 * comuna.getHeight() - y_tPlusDelta - 20;
            angle = -angle;
        }
    }

    public void updateState() {
        x = x_tPlusDelta;
        y = y_tPlusDelta;
        if((estado_tPlusDelta == 'i') && (infectadoTime ==-1)){
            initInfectadoTimer();
        }
        estado = estado_tPlusDelta;
    }

    public double distanceTo(Pedestrian individuo) {
        double xf = individuo.getX();
        double yf = individuo.getY();
        double dx = (xf - this.x);
        double dy = (yf - this.y);
        double distance = Math.sqrt(dx * dx + dy * dy);
        return distance;
    }

    public boolean contactoEstrecho(Pedestrian individuo) {
        if (distanceTo(individuo) <= SimulatorConfig.D) {
            return true;
        } else {
            return false;
        }
    }

    public static void Exposicion(Pedestrian individuo1, Pedestrian individuo2){
        double probabilidad_0 = SimulatorConfig.P0;
        double probabilidad_1 = SimulatorConfig.P1;
        double probabilidad_2 = SimulatorConfig.P2;
        if(individuo1.mascarilla && individuo2.mascarilla){
            makeExposicion(individuo1, individuo2, probabilidad_2);
        }
        if((!individuo1.mascarilla && individuo2.mascarilla) || (individuo1.mascarilla && !individuo2.mascarilla)){
            makeExposicion(individuo1, individuo2, probabilidad_1);
        }
        if(!individuo1.mascarilla && !individuo2.mascarilla){
            makeExposicion(individuo1, individuo2, probabilidad_0);
        }
    }

    public static void makeExposicion(Pedestrian individuo1, Pedestrian individuo2, double P){
        if(Math.random() < P){
            if(individuo1.infectado() && individuo2.suceptible()){
                individuo2.estado_tPlusDelta = 'i';
                individuo2.view.changeShape(individuo2.estado_tPlusDelta);
            }
            if(individuo2.infectado() && individuo1.suceptible()){
                individuo1.estado_tPlusDelta = 'i';
                individuo1.view.changeShape(individuo1.estado_tPlusDelta);
            }
        }
    }

}